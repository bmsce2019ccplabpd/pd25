#include<stdio.h>
void swap_callby_ref(int *,int *);
int main()
{
int a,b;
printf("enter the values of a and b \n");
scanf("%d \t %d",&a,&b);
printf("values of a and b are \n %d \t %d",a,b);
printf("\n values after  swapping a and b are");
swap_callby_ref(&a,&b);
return 0;
}
void swap_callby_ref(int *a,int *b)
{
int temp;
temp=*a;
*a=*b;
*b=temp;
printf("\n a=%d and b=%d",*a,*b);
}